// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	dir='p';
	//myfifo="/home/german/Escritorio/myfifo";
	//sc="/tmp/sc";
	//cs="/tmp/cs";
	memo="/tmp/memo";
	Init();
}

CMundo::~CMundo()
{	
/*
	sprintf(cad,"#Fin de Juego\n");
	write(fd,&cad,200);	
	close(fd);*/


	printf("#Fin de Juego\n");
	sprintf(cad,"#Fin de Juego\n");
	scom.Send(cad,200);
	scom.Close();	
	//write(fd_cs,cad,200);	
	data.sigue=0;//cerrar bot
	(*map)=data;
	munmap(map,sizeof(data));

/*
//destruccion canal s-c
	close(fd_sc);
	unlink(sc);
//destruccion canal c-s
	close(fd_cs);
	unlink(cs);
*/
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
/*
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	if(jugador1.Rebota(esfera) || jugador2.Rebota(esfera))
	{
		if (esfera.radio >= 0.2f)esfera.radio -= 0.1f;
	}
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		esfera.radio = 0.5f;
		puntos2++;
		//sprintf(cad,"Jugador 2 marca 1 punto, lleva %d puntos \n",puntos2);
		//write(fd,&cad,200);

	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		esfera.radio = 0.5f;
		puntos1++;
		//sprintf(cad,"Jugador 1 marca 1 punto, lleva %d puntos \n",puntos1);
		//write(fd,&cad,200);	
	}
	*/


//Comunicación s-c
	//read(fd_sc, &cad,200);
	scom.Receive(cad,200);
	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %f %d %d", &esfera.radio,&esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2); 
	sprintf(cad,"%c",dir);
	scom.Send(cad,200);

//bot
	data=(*map);
	if (data.accion==1)OnKeyboardDown('o',0,0);//jugador2.velocidad.y=4;
	else if (data.accion==-1)OnKeyboardDown('l',0,0);//jugador2.velocidad.y=-4;
	else jugador2.velocidad.y=0;

	data.esfera=esfera;
	data.raqueta1=jugador2;
	data.sigue=1;

	(*map)=data;
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	char cad[200];	
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':dir='s';break;
	case 'w':dir='w';break;
	case 'l':dir='l';break;
	case 'o':dir='o';break;
	}
	//printf("%s ",cad);
//comunicacion c-s
	//scom.Send(cad,200);
	//write(fd_cs,&cad,sizeof(cad));
}

void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
 
//mmap para el bot

//dar tamño al fichero con write o ftruncate
//utilizar data y simplemente actualizar puntero al mapa
	int fd;
	fd=open(memo,O_RDWR);
	if(fd==-1){
		perror("fichero memoria compartida mundo");
		exit(-1);

	}
	ftruncate(fd,sizeof(data));
	map=(DatosMemCompartida*)mmap(NULL,sizeof(data),PROT_WRITE|PROT_READ,MAP_SHARED,fd,(off_t)0);
	close(fd);
	if (map == MAP_FAILED){
	        perror ("mmap mundo");
		exit(-1);
	}
	map->sigue=1;

//Sockets
	printf("Abriendo Socket de comunicación\n");
	scom.Connect("127.0.0.1",7777);
	printf("Abierto scom\n");
	const char nombre[]="German";
	sprintf(cad,"%s",nombre);
	scom.Send(cad,200);

/*
	mkfifo(cs,0666);
	mkfifo(sc,0666);

//fifo s-c

	printf("abriendo fifo s-c\n");
	fd_sc = open (sc,O_RDONLY);
	if(fd_sc==-1){
		perror("fifo s-c en cliente");
		exit(-1);
	}printf("Creado y abierto fifo s-c\n");

//fifo c-s

	printf("abriendo fifo c-s\n");
	fd_cs = open (cs,O_WRONLY);
	if(fd_cs==-1){
		perror("fifo c-s en cliente");
		exit(-1);
	}printf("Creado y abierto fifo c-s\n");
*/
//fifo
/*
	fd = open(myfifo, O_WRONLY);
	if(fd==-1){
		perror("fifo tenis");
		exit(-1);

	}
*/	
}










