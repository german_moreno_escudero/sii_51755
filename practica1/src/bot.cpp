#include "DatosMemoriaCompartida.h"
#include <stdlib.h>
#include <stdio.h>
#include <sys/mman.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


int main(){
	DatosMemCompartida *map;
	int fd,i=0;
	const char *memo="/tmp/memo";
	printf("abriendo memo bot\n");
	fd=open(memo,O_RDWR|O_CREAT|O_TRUNC,0666);
	if(fd==-1){
		perror("fichero memoria compartida bot");
		exit(-1);

	}printf("abierto memo\n");
	ftruncate(fd,sizeof(DatosMemCompartida));
	map=(DatosMemCompartida*)mmap(NULL,sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE,MAP_SHARED,fd,(off_t)0);
	close(fd);
	if (map == MAP_FAILED){
	        perror ("mmap bot");
		exit(-1);
	}
	map->sigue=1;
	while(map->sigue!=0){
		//printf("sigue: %d\n",map->sigue);
		if(map->esfera.centro.y >= map->raqueta1.y2)map->accion=1;
		else if(map->esfera.centro.y <= map->raqueta1.y1)map->accion=-1;
		else if((map->esfera.centro.y < map->raqueta1.y2)&&(map->esfera.centro.y > map->raqueta1.y1))map->accion=0;
		usleep(25000);
		if(i<10)i++;
		
	}
	munmap(map,sizeof(DatosMemCompartida));
	unlink(memo);
}
